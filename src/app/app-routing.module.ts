import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddNotesDeFraisComponent } from './component/add-notes-de-frais/add-notes-de-frais.component';
import { AllNotesComponent } from './component/all-notes/all-notes.component';
import { HomeComponent } from './component/home/home.component';
import { LoginComponent } from './component/login/login.component';
import { MyNotesComponent } from './component/my-notes/my-notes.component';
import { RegisterComponent } from './component/register/register.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'home', component: HomeComponent },
  { path: 'add', component: AddNotesDeFraisComponent },
  { path: 'all', component: AllNotesComponent },
  { path: 'mynotes', component: MyNotesComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
