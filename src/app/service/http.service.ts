import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ClaimRequestDto } from '../model/claim-request-dto';
import { UserRequestDto } from '../model/user-request-dto';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  urlUser: string = "http://localhost:8080/user"
  urlClaim: string = "http://localhost:8081/api/claim"

  constructor(private http: HttpClient) {}

    verify(user: UserRequestDto): Observable<any> {
      return this.http.post(this.urlUser + "/verify", user)
    }

    register(user: UserRequestDto): Observable<any> {
      return this.http.post(this.urlUser + "/register", user)
    }

    login(user: UserRequestDto): Observable<any> {
      return this.http.post(this.urlUser + "/login", user)
    }




    getClaims(): Observable<any> {
      return this.http.get(this.urlClaim + "")
    }

    getClaimById(id: number): Observable<any> {
      return this.http.get(this.urlClaim + "/" + id)
    }

    getClaimByIdUser(id: number): Observable<any> {
      return this.http.get(this.urlClaim + "/user/" + id)
    }

    createClaim(claim: ClaimRequestDto): Observable<any> {
      return this.http.post(this.urlClaim + "", claim)
    }

    updateStatus(id: number): Observable<any> {
      return this.http.put(this.urlClaim + "/status/", id)
    }

}
