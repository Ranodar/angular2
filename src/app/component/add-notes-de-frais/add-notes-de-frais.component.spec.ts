import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNotesDeFraisComponent } from './add-notes-de-frais.component';

describe('AddNotesDeFraisComponent', () => {
  let component: AddNotesDeFraisComponent;
  let fixture: ComponentFixture<AddNotesDeFraisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddNotesDeFraisComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddNotesDeFraisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
