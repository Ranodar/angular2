import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserRequestDto } from 'src/app/model/user-request-dto';
import { HttpService } from 'src/app/service/http.service';
import { LoginService } from 'src/app/service/login.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  user: UserRequestDto;
  formRegister: FormGroup;

  constructor(private fb: FormBuilder, private http: HttpService, private router: Router, private loginService: LoginService) { }

  ngOnInit(): void {
    this.formRegister = this.fb.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]]
    })
  }

  register() {
    if (this.formRegister.valid) {
      this.user = this.formRegister.value
      this.http.login(this.user)
      this.router.navigate(["/home"])
    }
  }

  get email() {
    return this.formRegister.get("email")
  }

  get password() {
    return this.formRegister.get("password")
  }

}
