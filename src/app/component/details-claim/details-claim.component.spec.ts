import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsClaimComponent } from './details-claim.component';

describe('DetailsClaimComponent', () => {
  let component: DetailsClaimComponent;
  let fixture: ComponentFixture<DetailsClaimComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailsClaimComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DetailsClaimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
