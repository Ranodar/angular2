import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Claim } from 'src/app/model/claim';
import { ClaimRequestDto } from 'src/app/model/claim-request-dto';
import { HttpService } from 'src/app/service/http.service';

@Component({
  selector: 'app-details-claim',
  templateUrl: './details-claim.component.html',
  styleUrls: ['./details-claim.component.scss']
})
export class DetailsClaimComponent implements OnInit {

  formClaim: FormGroup;
  claim: ClaimRequestDto
  post: Claim | undefined;

  constructor(private http: HttpService, private route: Router, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.formClaim = this.fb.group({
      date: ["", Validators.required],
      company: ["", Validators.required],
      price: ["", Validators.required],
      category: ["", Validators.required],
    })
  }



  get date() {
    return this.formClaim.get('date');
  }
  get company() {
    return this.formClaim.get('company');
  }
  get price() {
    return this.formClaim.get('price');
  }
  get category() {
    return this.formClaim.get('category');
  }


  addClaim() {
    this.claim = this.formClaim.value
    this.http.createClaim(this.claim).subscribe(x => {
     })
     this.route.navigate(["/mylist"])
  }


}
