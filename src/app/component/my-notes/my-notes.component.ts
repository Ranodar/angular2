import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClaimResponseDto } from 'src/app/model/claim-response-dto';
import { HttpService } from 'src/app/service/http.service';

@Component({
  selector: 'app-my-notes',
  templateUrl: './my-notes.component.html',
  styleUrls: ['./my-notes.component.scss']
})
export class MyNotesComponent implements OnInit {

  claims: ClaimResponseDto[]

  constructor(private http: HttpService, private router: Router) { }

  getClaims() {
    this.http.getClaims().subscribe(x => {
      this.claims = x
    })
  }

  ngOnInit(): void {
    this.getClaims()
  }

  updateStatus(id: number){
    this.http.updateStatus(id)
  }

}
