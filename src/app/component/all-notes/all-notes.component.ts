import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ClaimResponseDto } from 'src/app/model/claim-response-dto';
import { HttpService } from 'src/app/service/http.service';

@Component({
  selector: 'app-all-notes',
  templateUrl: './all-notes.component.html',
  styleUrls: ['./all-notes.component.scss']
})
export class AllNotesComponent implements OnInit {

  claims: ClaimResponseDto[]

  constructor(private http: HttpService, private router: Router) { }

  getClaims() {
    this.http.getClaims().subscribe(x => {
      this.claims = x
    })
  }

  ngOnInit(): void {
    this.getClaims()
  }

  updateStatus(id: number){
    this.http.updateStatus(id)
  }

}
