export enum Status {
  PENDING,
  VALIDATED,
  REFUND
}
