import { Claim } from "./claim"

export class Proof {

  public id: number
  public url: string
  public claim: Claim

  constructor(url?: string, claim?: Claim) {
    this.url = url || '';
    this.claim = claim || null;
  }

}
