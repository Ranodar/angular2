import { Proof } from "./proof"
import { Status } from "./status"

export class ClaimResponseDto {

  public id: number
  public date: string
  public category: string
  public company: string
  public price: number
  public status: Status
  public proofs: Proof[]

  constructor(date?: string, category?: string, company?: string, price?: number, proofs?: Proof[]) {
    this.date = date || '';
    this.category = category || '';
    this.company = company || '';
    this.price = price || null;
    this.proofs = proofs || null;
  }
}
