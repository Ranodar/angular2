export class UserResponseDto {

  public id: number;
  public email: string;
  public password: string;
  public isAdmin: boolean

  constructor(email?: string, password?: string) {
    this.email = email || '';
    this.password = password || '';
  }


}
